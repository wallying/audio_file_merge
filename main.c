#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "wallyingLib.h"
#include "cJSON.h"



#define LO_UINT16(x)        ((uint8_t)(((x) >> 0) & 0xFF))
#define HI_UINT16(x)        ((uint8_t)(((x) >> 8) & 0xFF))
#define BUILD_UINT16(byte0, byte1) \
                            ((uint16_t)((((uint16_t)(byte0) & 0xFF) << 0) + \
                                        (((uint16_t)(byte1) & 0xFF) << 8)))


#define BYTE0_UINT32(x)     ((uint8_t)(((x) >>  0) & 0xFF))
#define BYTE1_UINT32(x)     ((uint8_t)(((x) >>  8) & 0xFF))
#define BYTE2_UINT32(x)     ((uint8_t)(((x) >> 16) & 0xFF))
#define BYTE3_UINT32(x)     ((uint8_t)(((x) >> 24) & 0xFF))
#define BUILD_UINT32(byte0, byte1, byte2, byte3) \
                            ((uint32_t)((((uint32_t)(byte0) & 0xFF) <<  0) + \
                                        (((uint32_t)(byte1) & 0xFF) <<  8) + \
                                        (((uint32_t)(byte2) & 0xFF) << 16) + \
                                        (((uint32_t)(byte3) & 0xFF) << 24)))

#if 0
/**
 * @brief : hexstring_to_hexnumber
 * @param  str_buf :输入要转换的字符串数组
 * @param  str_len : 输入要转换的长度
 * @param  hex_buf :输出转换后的hex数组
 * @return unsigned int :输出转换后的hex数组长度
 */
uint8_t str_to_hex(char *str_buf, uint8_t str_len, uint8_t *hex_buf)
{
    if (!str_buf || !hex_buf) {
        return 0;
    }

    uint8_t hex_u8, hex_len = 0;

    for (uint8_t i = 0; i < str_len; i++)
    {
        if ((str_buf[i] >= '0') && (str_buf[i] <= '9')) {
            hex_u8 = str_buf[i] - '0';
        } else if ((str_buf[i] >= 'A') && (str_buf[i] <= 'F')) {
            hex_u8 = str_buf[i] - 'A' + 10;
        } else if ((str_buf[i] >= 'a') && (str_buf[i] <= 'f')) {
            hex_u8 = str_buf[i] - 'a' + 10;
        } else {
            hex_u8 = 0xFF;
        }

        if (hex_u8 != 0xFF)
        {
            if (i % 2 == 0)
            {
                hex_buf[i / 2] = hex_u8 << 4;
                hex_len++;
            }
            else
            {
                hex_buf[i / 2] |= hex_u8;
            }
        }
        else
        {
            return 0;
        }
    }

    return hex_len;
}
#endif

uint32_t str_to_hex(char *str_buf, uint8_t str_len)
{
    if (str_buf == NULL) {
        return 0x00;
    }

    uint8_t i, hex_u8;
    uint32_t hex_out;

    hex_out = 0x00;
    for (i = 0; i < str_len; i++)
    {
        if ((str_buf[i] >= '0') && (str_buf[i] <= '9')) {
            hex_u8 = str_buf[i] - '0';
        } else if ((str_buf[i] >= 'A') && (str_buf[i] <= 'F')) {
            hex_u8 = str_buf[i] - 'A' + 10;
        } else if ((str_buf[i] >= 'a') && (str_buf[i] <= 'f')) {
            hex_u8 = str_buf[i] - 'a' + 10;
        } else {
            hex_u8 = 0xFF;
            break;
        }

        hex_out <<= 4;
        hex_out += hex_u8;
    }

    return hex_out;
}


typedef struct {
    FILE *fp;
    char name[255];
    char *buf;
    uint32_t len;
} file_info_t;

int file_open(file_info_t *file, char *name, char *mode)
{
    memset(file, 0x00, sizeof(file_info_t));

    strncpy(file->name, name, sizeof(file->name) - 1);

    file->fp = fopen(file->name, mode);
    if (file->fp == NULL) {
        return -2;
    }
    fseek(file->fp, 0L, SEEK_END);
    file->len = ftell(file->fp);
    fseek(file->fp, 0L, SEEK_SET);

    file->buf = malloc(file->len);
    if (file->buf == NULL) {
        fclose(file->fp);
        return -3;
    }
    memset(file->buf, 0x00, file->len);
    fread(file->buf, 1, file->len, file->fp);

    return 0;
}

int file_close(file_info_t *file)
{
    if (file->buf) {
        free(file->buf);
    }

    if (file->fp) {
        fclose(file->fp);
    }

    return 0;
}


#define VOICE_HEADER_LEN      (256)

#pragma pack(1)
typedef struct {
    uint8_t voice_id;       /**< 单个音频ID */
    uint32_t voice_addr;    /**< 单个音频地址 */
    uint16_t voice_len;     /**< 单个音频长度 */
    uint16_t reserve;       /**< 预留2个字节 */
} ty_voice_single_t;

typedef struct {
    uint8_t amount;         /**< 音频个数 */
    uint32_t total_len;     /**< 音频总长度 */
    ty_voice_single_t single[30];
} ty_voice_header_t;
#pragma pack()

ty_voice_header_t voice_header;
uint32_t flash_addr = 0x00;
uint32_t wave_addr;


file_info_t merge_json;
file_info_t merge_wave;
file_info_t merge_file;


int main(int argc, char *argv[])
{
//    printf("Hello World! --- cJSON: %s\r\n", cJSON_Version());

    int ret;

    memset(&voice_header, 0x00, sizeof(ty_voice_header_t));

    ret = file_open(&merge_json, "merge_json.json", "r");
    if (ret != 0) {
        printf(" ERROR: no \"%s\" file!!!\r\n", merge_json.name);
        exit(1);
    }

    ret = file_open(&merge_wave, "merge_wave.bin", "wb+");
    if (ret != 0) {
        printf(" ERROR: no \"%s\" file!!!\r\n", merge_wave.name);
        exit(1);
    }

    ret = file_open(&merge_file, "merge_file.bin", "wb+");
    if (ret != 0) {
        printf(" ERROR: no \"%s\" file!!!\r\n", merge_file.name);
        exit(1);
    }


    /*==============================================*/
    cJSON *json;

    json = cJSON_Parse(merge_json.buf);
    if (json == NULL) {
        printf("JSON ERROR => %s\r\n", cJSON_GetErrorPtr());
    }



    cJSON *flashAddr;
    flashAddr = cJSON_GetObjectItem(json, "flashAddrHex");
    if (cJSON_IsString(flashAddr)) {
        printf("flashAddr:%s\r\n", flashAddr->valuestring);

        wave_addr = str_to_hex(flashAddr->valuestring, strlen(flashAddr->valuestring));
//        flash_addr += VOICE_HEADER_LEN;
    }

    //将固件写入到 merge_file 文件中
    cJSON *firmware;
    firmware = cJSON_GetObjectItem(json, "firmware");
    if (cJSON_IsString(firmware))
    {
        printf("firmware:%s\r\n", firmware->valuestring);

        file_info_t temp_file;
        ret = file_open(&temp_file, firmware->valuestring, "rb");
        if (ret != 0)
        {
            printf(" ERROR: no \"%s\" file!!!\r\n", temp_file.name);
            return 1;
        }

        flash_addr += temp_file.len;

        fwrite(temp_file.buf, 1, temp_file.len, merge_file.fp);

        file_close(&temp_file);
    }


    //预留空间且写入 0xFF
    uint32_t reserve_len;
    uint8_t *reserve_buf;
    reserve_len = wave_addr - flash_addr;
    reserve_buf = malloc(reserve_len);
    if (reserve_buf == NULL) {
        return 1;
    }
    memset(reserve_buf, 0xFF, reserve_len);
    fwrite(reserve_buf, 1, reserve_len, merge_file.fp);
    free(reserve_buf);



    cJSON *fileList, *fileName;
    fileList = cJSON_GetObjectItem(json, "fileList");
    if (cJSON_IsArray(fileList))
    {
        voice_header.amount = cJSON_GetArraySize(fileList);

        voice_header.total_len = 0;
        fseek(merge_wave.fp, VOICE_HEADER_LEN, SEEK_SET);
        wave_addr += VOICE_HEADER_LEN;

        for (int i = 0; i < voice_header.amount; i++)
        {
            fileName = cJSON_GetArrayItem(fileList, i);
            if (cJSON_IsString(fileName))
            {
//                printf("fileName:%s\r\n", fileName->valuestring);

                file_info_t temp_wave;
                ret = file_open(&temp_wave, fileName->valuestring, "rb");
                if (ret != 0)
                {
                    printf(" ERROR: no \"%s\" file!!!\r\n", temp_wave.name);
                    return 1;
                }

                {
                    voice_header.total_len += temp_wave.len;

                    voice_header.single[i].voice_id = i + 1;
                    voice_header.single[i].voice_addr = wave_addr;
                    voice_header.single[i].voice_len = temp_wave.len;

                    wave_addr += temp_wave.len;

                    fwrite(temp_wave.buf, 1, temp_wave.len, merge_wave.fp);
                }
                file_close(&temp_wave);
            }
        }

    }


    //
    uint8_t headBuf[VOICE_HEADER_LEN];

    fseek(merge_wave.fp, 0L, SEEK_SET);
    memset(headBuf, 0x00, sizeof(headBuf));
    headBuf[0] = voice_header.amount;
    headBuf[1] = BYTE0_UINT32(voice_header.total_len);
    headBuf[2] = BYTE1_UINT32(voice_header.total_len);
    headBuf[3] = BYTE2_UINT32(voice_header.total_len);
    headBuf[4] = BYTE3_UINT32(voice_header.total_len);
    for (int i = 0; i < voice_header.amount; i++) {
        headBuf[5 + i * 9] = voice_header.single[i].voice_id;
        headBuf[6 + i * 9] = BYTE0_UINT32(voice_header.single[i].voice_addr);
        headBuf[7 + i * 9] = BYTE1_UINT32(voice_header.single[i].voice_addr);
        headBuf[8 + i * 9] = BYTE2_UINT32(voice_header.single[i].voice_addr);
        headBuf[9 + i * 9] = BYTE3_UINT32(voice_header.single[i].voice_addr);
        headBuf[10 + i * 9] = LO_UINT16(voice_header.single[i].voice_len);
        headBuf[11 + i * 9] = HI_UINT16(voice_header.single[i].voice_len);
    }
    fwrite(headBuf, 1, sizeof(headBuf), merge_wave.fp);




    //将 merge_wave 文件拷贝到 merge_file 文件的特定地址中
    fseek(merge_wave.fp, 0L, SEEK_SET);
    uint8_t copy_buf[4 * 1024];
    uint32_t copy_len;
    while ((copy_len = fread(copy_buf, 1, sizeof(copy_buf), merge_wave.fp)) > 0) {
        fwrite(copy_buf, 1, copy_len, merge_file.fp);
    }












    cJSON_Delete(json);


    /*==============================================*/
    file_close(&merge_json);
    file_close(&merge_wave);
    file_close(&merge_file);


    system("pause");
    return 0;
}
